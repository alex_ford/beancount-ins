# Beancount Import and Sync (`beancount-ins`)

This project builds off of the Beancount double-entry accounting CLI project. <http://furius.ca/beancount/>

Import and Sync (INS) is a collection of Python code and shell scripts which deal with the problem of importing and synchronizing data for Beancount data file(s).

By convention, the scripts will expect data to be dropped into the ./import-files/ subdirectory. Create it if it doesn't exist! The file names need to match what the respective script is looking for. Some scripts may also provide an option to pass the file in as an argument.

**Project Maturity**: 2 - Prototype

<details>
Maturity is is a linear scale that attempts to report the readiness of the artifact (project/script/etc.) for use by the intended end-users (e.g. the public).

<pre>[ 1 - Ideation ] -> [ 2 - Prototype ] -> [ 3 - Alpha ] -> [ 4 - Beta ] -> [ 5 - Generally Releasable ]</pre>

<i>Ideation</i> is the first phase, and simply means the artifact is still just a bunch of ideas. There is likely not even any code, or other implementation yet.

<i>Prototype</i> is the second phase, and means implemenation has begun (e.g. code) but is very rough, likely examples and other snippets which haven't been designed very well.

<i>Alpha</i> is the third phase, and means much of the core functionality has been implemented. The artifact is usable, but likely has limitations/restrictions and bugs that need to be fixed.

<i>Beta</i> is the fourth phase, and means we almost have a finished artifact! The focus now should shift to testing and polishing activities.

<i>Generally Releasable</i> is the fifth (and final) phase, and means we are 'done'! At least to the intended definition of the artifact. This doesn't mean changes won't happen in the future, but simply means the artifact is mature enough that the target end-users are able to use it fully and with reasonable expectation that it won't crash/fail.

</details>

**Author**: Alex Richard Ford (arf4188@gmail.com)

**Website**: <http://www.alexrichardford.com>

**License**: MIT License (see [LICENSE](./LICENSE))

## Project Structure

Here is a short description of the important files and directories in this project:

- `d ./beancount_ins/`
    - Python package containing scripts which do *not* integrate with Beancount's plugin framework.
- `d ./config/`
    - Configuration files for this project.
- `d ./docs/`
    - Documentation for the project.
- `d ./import-files/`
    - Directory reserved for files downloaded by the user and consumed by INS code.
- `d ./importers/`
    - Contains Python code for importers that do integrate with Beancount's plugin framework.
- `d ./scripts/`
    - Supporting install/run scripts for various operating systems/platforms.
- `d ./secrets/`
    - Directory reserved for sensitive files that the user might need to provide, like API keys/tokens, passwords, etc.
- `d ./tests/`
    - Test code is stored here.
- `f ./beancount-ins.code-workspace`
    - Visual Studio Code workspace for developing `beancount-ins`.
- `f ./LICENSE`
    - Full legal LICENSE text can be found here.
- `f ./Pipfile`
    - Pipenv file declaring the dependencies for both user runtime and development of the project.
- `f ./README.md`
    - Primary documentation entry-point for the project. You're reading this right now!
- `f ./VERSION`
    - Contains the version number of the project.

## Dependencies

In addition to this Git repo, you will also need the following other repos:

- `beancount-arfsrc`

And the following Python packages available from PyPi:

- `beancount`
- `arrow`
- `colorlog`
- `requests`
- `lxml`
- `ak-gchartwrapper`

The full list of package dependencies can be found in the `./Pipfile` file.

## Categories of Import and Sync

In this project, I've defined a handful of very specific categories dealing with the whole general notion of importing and syncing data. Depending on what you want, one category will be more useful than the others. Here's the general scheme:

- download-[institution]-[filetype].py
 - Download scripts are used to automatically fetch data from the named [institution] and help speed up this first part of the process.
- import-[institution]-[filetype].py
 - These Python scripts will consume the CSV export produced by the named [institution] in the script name. The default behavior is to simply dump the results to stdout. They will also generally follow my convention of providing a --save argument which can be used to save the output directly to your Beancount data file.
- sync-[institution].py
 - For the sync Python scripts, these attempt to do a bit more... or should they?
- update-[description].py
 - Update scripts simply fetch new data from online data sources. By default, they write the results to stdout, but generally support the --save argument as the other scripts do.

## Quick Start Usage

1. Install Python 3.6+ and Pipenv

    ```shell
    sudo apt-get install python3
    python3 -m pip install pipenv
    ```

2. Start a Python Virtual Environment in the `beancount-ins` directory.

    ```shell
    pipenv install
    ```

3. If you had to use the first method above, then build the `beancount-arfsrc` project next:

    ```shell
    pipenv run python ../beancount-arfsrc/setup.py install
    ```

4. Execute the desired script, for example: 

    ```shell
    pipenv run python ./ins/update-all.py
    ```

5. If you would like to save the output to your Beancount data file, do this: 

    ```shell
    pipenv run python ./ins/update-all.py --save datafile.beancount
    ```

## Common Conventions

In all of my beancount-\* repos, I follow the following conventions:

- Beancount data files (with your actual transactions in them) use the .bcdat extension
- Beancount spec files (which are supporting files used by my scripts and plugins) use the .bcspec extension
- Configuration files use the .config extension, and typically control configuration settings that span across multiple scripts
- Standalone Python scripts (that are meant to be run outside of Beancount/Fava) will typically provide a `--help` argument that can assist in usage.
 - One common argument these scripts provide is the `--save` argument, which takes a filename/path and attempts to save the result of the script to the specified Beancount data file.

## Resources

The Beancount community has the "[Importing External Data](https://docs.google.com/document/d/11EwQdujzEo2cxqaF5PgxCEZXWfKKQCYSMfdJowp_1S8/edit)" Google Doc with some good info:

