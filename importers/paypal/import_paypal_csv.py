#! python

# @author Alex Richard Ford (arf4188@gmail.com)
# @website http://www.alexrichardford.com
# @license MIT License (see LICENSE.md)

from beancount_entry import BeancountEntry
from beancount_metadata import BeancountMetadata
from beancount_posting import BeancountPosting

import argparse
import sys
import logging
from csv import DictReader

logging.basicConfig(level=logging.DEBUG)
_log = logging.getLogger(sys.argv[0])


def main():
    argparser = argparse.ArgumentParser(
        description="PayPal CSV to Beancount converter.")
    argparser.add_argument("inputCsvFile")
    args = argparser.parse_args()

    with open(args.inputCsvFile) as csvFile:
        headerKeys = [h.strip(" \"") for h in csvFile.readline().split(',')]
        reader = DictReader(csvFile, fieldnames=headerKeys)
        _log.debug("CSV header keys: {}".format(reader.fieldnames))
        for line in reader:
            _log.debug(line)
            bce = BeancountEntry()
            bce.setDate(line['Date'])
            bce.setFlag("*")
            bce.setPayee(line['Name'])
            bce.setDescription(line['Type'])

            # Metadata
            bce.addMetadata(BeancountMetadata(
                key="time",
                value="{} {}".format(line['Time'], line['TimeZone'])))
            bce.addMetadata(BeancountMetadata(
                key="statement-payee",
                value=bce.Payee
            ))
            bce.addMetadata(BeancountMetadata(
                key="statement-desc",
                value=bce.Description
            ))
            bce.addMetadata(BeancountMetadata(
                key="generated",
                value=True))
            bce.addMetadata(BeancountMetadata(
                key="paypal-txid",
                value=line['Transaction ID']))
            if line['Note'] is not "":
                bce.addMetadata(BeancountMetadata(
                    key="paypal-note",
                    value=line['Note']
                ))
            if line['Item Title'] is not "":
                bce.addMetadata(BeancountMetadata(
                    key="paypal-item-title",
                    value=line['Item Title']
                ))

            # Posting 1
            bcp1 = BeancountPosting()
            bcp1.Account = "Assets:PayPal"
            bcp1.AmountValue = float(line['Gross'].replace(",", ""))
            bcp1.AmountUnit = "USD"
            bce.addPosting(bcp1)

            # Posting 2
            bcp2 = BeancountPosting()
            bce.addPosting(bcp2)

            # Debug printout
            # This is purposely a 'print' instead of using _log so that we
            # can pipe the output to a file and have it be valid beancount data
            print("{}".format(bce))


if __name__ == '__main__':
    main()
