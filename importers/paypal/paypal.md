# PayPal

- Head on over to https://business.paypal.com/merchantdata/reportHome?reportType=DLOG
- Select `All transactions`, select desired date range (e.g. 3 months), select `csv` format
- Then <button>Create Report</button>
- They say the report can take some time and they will email when ready, but I've found the report prepares quickly and is available to download right from that same page in about 10-20 seconds.
- You'll then need to convert the file encoding from `UTF-8 with BOM` to plain `UTF-8` for best results.

![Screenshot from 2018-10-27 19-07-48](./res/Screenshot from 2018-10-27 19-07-48.png)

<div style="border-style: solid; border-size: 5px; border-color: grey;">
    <div style="background: #FFAA55; padding: 5px;">
        <b>IMPORTANT!</b>
    </div>
    <div style="background: #EEEEEE; padding: 10px;">
        If trying to access your transaction history from the main PayPal website, you might not see the triple line menu button if your browser window is too small!
    </div>
</div>



# TODO

- [ ] TODO: the `csv-to-beancount.py` script does not handle fees at all
    - An input like this:
        ```csv
        "07/28/2018","21:38:11","EDT","Brian Turner","eBay Auction Payment","Completed","USD","82.20","-2.68","79.52","To get contact details, please visit your Order details on MyeBay","arf4188@gmail.com","54R598438L024064F","Brian, Turner, 426 Chandelle Ct NE, Salem, OR, 97301, United States","Confirmed","Microsoft Surface Book Docking Station","263833674707","7.20","0.00","0.00","","","","","","","EBAY_EMSCX0000822459426816","1","2231785371095319","79.52","426 Chandelle Ct NE","","Salem","OR","97301","United States","","Microsoft Surface Book Docking Station","","US","Credit"
        ```
    - Should end up like this:
        ```beancount
        2018-07-28 * "Brian Turner" "eBay Auction Payment"
        statement-payee: "Brian Turner"
        statement-desc: "eBay Auction Payment"
        time: "21:38:11 EDT"
        generated: TRUE
        paypal-txid: "54R598438L024064F"
        paypal-item-title: "Microsoft Surface Book Docking Station"
        Assets:PayPal                                                                  79.52 USD
        Expenses:Services:Market-Membership-Fees                                        2.68 USD
        ! UNKNOWN                                                                     -82.20 USD
        ```
