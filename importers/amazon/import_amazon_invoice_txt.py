#!/usr/env python3

"""extract.py

This is an attempt to extract useful data from a text-copy of Amazon invoices.

Status: this seems to work over the single test case I've tried it on, but
obviously needs to be put through some paces with more test cases before it can
be confirmed as fully viable.

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

import arrow

order_date = None
order_total = None
order_number = None
items: list = list()

with open("./20181214_Amazon_111-0410986-6196260.txt", "r") as f:
    for l in f:
        _log.debug(l)
        if l.startswith("Amazon.com order number"):
            _log.debug("----------------------->> USING THIS AS 'order_number'")
            order_number = l.split(":")[1].strip()
        if l.startswith("Order Placed:"):
            _log.debug("----------------------->> USING THIS AS 'order_date'")
            order_date = arrow.get(l.split(":")[1].strip(), "MMMM DD, YYYY")
        if l.startswith("Order Total:"):
            _log.debug("----------------------->> USING THIS AS 'order_total'")
            order_total = l.split(":")[1].strip()
        if l.startswith(" of: ", 1):
            _log.debug("----------------------->> USING THIS AS 'item'")
            i = dict()
            i['desc'] = l.split(":")[1].strip()
            items.append(i)
        if l.startswith("Condition:"):
            # figure out how to deal with multi lines
            price = next(f).strip()
            i['price'] = price

print("{} ! \"Amazon\"  \"Online Shopping\"".format(order_date.date()))
print("  order-number: \"{}\"".format(order_number))
print("  Liabilities:Credit-Cards:Chase-Sapphire-Reserve    -{} USD".format(order_total))
for i in items:
    print("  Expenses:???        {} USD".format(i['price']))
    print("    desc: \"{}\"".format(i['desc']))
