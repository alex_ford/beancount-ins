import os, sys

"""

https://gist.github.com/mterwill/7fdcc573dc1aa158648aacd4e33786e8
"""

# beancount doesn't run from this directory
sys.path.append(os.path.dirname(__file__))

# importers located in the importers directory
# from importers import amex, chase, schwab, citi
from chase import chase

# CONFIG = [
#     chase.ChaseCCImporter('Liabilities:CC:Chase:Reserve', '0000'),
#     chase.ChaseBankImporter('Assets:Chase:Checking', '0000'),
#     schwab.SchwabBankImporter('Assets:Schwab:Checking', '8000'),
# ]
CONFIG = [
    chase.ChaseCCImporter('Liabilities:Credit-Cards:Chase-Sapphire-Reserve', '0000'),
    # chase.ChaseBankImporter('Assets:Chase:Checking', '0000'),
    # schwab.SchwabBankImporter('Assets:Schwab:Checking', '8000'),
]
