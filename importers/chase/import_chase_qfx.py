import codecs
from ofxparse import OfxParser
from qfx
with codecs.open('C:\\Users\\arf41\\Downloads\\Chase2480_Activity_20180615.qfx') as fileobj:
    ofx = OfxParser.parse(fileobj)
ofx.accounts                        # An account with information
ofx.account.number                  # The account number
ofx.account.routing_number          # The transit id (sometimes called branch number)
ofx.account.statement               # Account information for a period of time
ofx.account.statement.start_date    # The start date of the transactions
ofx.account.statement.end_date      # The end date of the transactions
ofx.account.statement.transactions  # A list of account activities
ofx.account.statement.balance       # The money in the account as of the statement date
ofx.account.statement.available_balance # The money available from the account as of the statement date