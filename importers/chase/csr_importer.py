from beancount.ingest import importer

class Importer(importer.ImporterProtocol):

	def name(self):
		return "CSR"

	def identify(self, file):
		pass

	def extract(self, file, existing_entries=None):	
