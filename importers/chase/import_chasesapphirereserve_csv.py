#!/usr/env python3

# MATURITY LEVEL: 2 (Alpha-1)

"""
The Chase Sapphire Reserve (CSR) CSV importer. Simply log into your Chase credit card online
account, downloading the CSV version of your transactions, and feed it into this script!

Example:
```shell
pipenv run python ./importers/chase-sr/import_chasesapphirereserve_csv.py --file chase-activity.csv
```

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

##################
# FEATURES & TODOs
##################

# - [x] Basic parsing of CSR CSV files to extract date, payee, narration, and
#           amount fields.

# - [x] Help switch which provides a quick start description. (--help)

# - [x] Save switch to save the results to the default beancount ledger file (--save)

# - [ ] Ability to optionally specify the beancount ledger file (--save bcfile)

# - [ ] Match and update with existing transactions in beancount file

# - [ ] Interleve transactions into beancount file by date

# - [ ] Interactive mode that allows user to specify various fields

# - [x] Regex rule matching system to replace "dirty"/"ugly" values. This is for the payee field.
#       This could be like a list of regexs stored in another file which
#       if matched, resolves to the beancount account specified. The rules
#       would be processed in order and first match is used. So, if you had
#       just the "*" wildcard at the top of the file, all your other rules
#       would be ignored. (Probably not your intent!)
#       Your rules file is considered personal/private data, so keep it in your beancount-data
#       directory, out of public view!
#       Rules are simple:
#           .*AMAZON.* = Amazon
#       The above will use "Amazon" whenever it detects the indicated regex.

# - [ ] Regex rule matching for the description field. (See above for common details.)

# - [ ] Regex rule matching for the 2nd posting account. (See above for common details.)

# - [x] Switches to control start/end dates read from source/input file. This is
#       useful when your input file contains way more data than you care about, and
#       want to limit what you import based on a date range.
#       Use --start YYYY-MM-DD or --end YYYY-MM-DD to set the appropriate value. Both
#       are optional. If neither are specified, the entire input file is used. If only
#       the --start option is specified, then only data as early as --start will be
#       included, along with all data in the future. Likewise, if --end is specified,
#       all earlier data is included, up to the --end date. If both are specified, then
#       the data used is bounded within that range.

# - [x] Use a real logging library instead of just print() statements.
#       Used the logging module part of standard python SDK. To configure the logging
#       level and destination, see the configuration section near the top of the script.

# - [ ] DEFECT: Chase allows commas in the Description column in their CSV export, but they
#       do not bother to use quotes to denote the difference between a comma that is part of
#       the description, versus a comma that represents the CSV delimiter.
#       Sample Input Data:
#       Sale,06/05/2018,06/07/2018,CITYSIDE SUBARU, INC,-2011.48
#       Currently, the script reads in " INC" as the value, instead of "-2011.48"
#       Solution: the columns are actually well formed in this case, so we could use a regex
#       to determine if an errant comma is a delimiter or not. That is, when we're reading in
#       the description text, we only break on the comma that immediately precedes a numerical
#       value.

# - [x] Include the Type, Trans Date, and Post Date as metadata

# - [ ] Use arfsrc beancount data models instead of custom string manipulation.

# - [ ] Ability to determine/aprox "statement tag" based on user configuration
#       My convention for tracking statements involves using tags, something like #CSR2018-0506
#       The transaction CSV doesn't tell us explicitly which statement the tx
#       was posted to, but we can ask the user when their statement closing date is.
#       If given as such "15th" (of each month), then the script should be able to compute
#       the rest!

# - [x] Include an #imported tag to help indicate the difference between imported transactions
#       versus manual transactions.

# - [ ] Provide config option to enable/disable #imported tag feature

import colorlog
import arrow
import csv, datetime, argparse, re

_log = colorlog.getLogger(__name__)

DEFAULT_CATEGORY = "! Expenses:Other"
DEFAULT_ACCOUNT = "Liabilities:Credit-Cards:Chase-Sapphire-Reserve"

# path to the main beancount file
beancountFile = "../beancount-data/csr.beancount"

# a rules file that helps translate 'input payees' to more preferred values
payeeRulesFile = "../beancount-data/configuration/ins-payee-rules.bcspec"

# the path to the CSR transactions CSV file
inputCsvFile = "../beancount-data/import-files/inbox/csr_transactions.csv"

# input files will be moved here after the import
processedInputDir = "../beancount-data/import-files/processed/"

# output files will be organized with names of the form "YYYYMM.beancount", 
# with each file only containing transactions for the appropriate month.
outputDir = "../beancount-data/import-files/output/"

def main():
    parser = argparse.ArgumentParser(
        description="Allows you to import Beancount transactions from a "
                    "source CSV file exported from the Chase Sapphire "
                    "Reserve website."
    )
    parser.add_argument("-s", "--save",
        help="Saves the results to the beancount ledger file.",
        action="store_true")
    parser.add_argument("-f", "--file",
        help="Uses the specified file as the input to this utility.",
        default="./import-files/csr-transactions.csv")
    parser.add_argument("--start",
        help="Specify a starting date to bound the data imported from the input file. Use YYYY-MM-DD format.",
        default=None)
    parser.add_argument("--end",
        help="Specify a starting date to bound the data imported from the input file. Use YYYY-MM-DD format.",
        default=None)
    args = parser.parse_args()

    _log.info("Using input file: " + args.file)

    if args.start != None:
        # Validation check
        if re.match(r"\d{4}-\d{2}-\d{2}", args.start):
            _log.info("Using start date of: " + args.start)
        else:
            _log.critical("Invalid format for --start option value.")
            exit(-1)
    if args.end != None:
        # Validation check
        if re.match(r"\d{4}-\d{2}-\d{2}", args.end):
            _log.info("Using end date of: " + args.end)
        else:
            _log.critical("Invalid format for --end option value.")
            exit(-1)

    # Load up the Rules files
    payeeRulesTuplesList = []
    with open(payeeRulesFile, "r") as prf:
        for line in prf:
            if line.startswith("#") or line.startswith("\n"):
                continue
            tokens = line.split("=")
            payeeRulesTuplesList.append((tokens[0].strip(), tokens[1].strip()))
    print(payeeRulesTuplesList)

    # Basic beancount transactions look like this:
    #     2017-09-14 * "Chase" "CSR CC payment"
    #         Assets:Banking:Ally:Checking                                               -3204.00 USD
    #         Liabilities:Credit-Cards:Chase-Sapphire-Reserve
    skipOld = 0
    skipNoAccount = 0
    skippedMintAccounts = set()
    aggregatedBeancountArray = []
    with open(args.file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            dateObj = datetime.datetime.strptime(line['Transaction Date'], "%m/%d/%Y")
            dateStr = dateObj.strftime("%Y-%m-%d")
            # Check against start/end if needed
            if args.start != None and datetime.datetime.strptime(args.start, "%Y-%m-%d") > dateObj:
                _log.debug("Skipping transaction from " + dateStr + " since it comes before --start option.")
                continue
            if args.end != None and datetime.datetime.strptime(args.end, "%Y-%m-%d") < dateObj:
                _log.debug("Skipping transaction from " + dateStr + " since it comes after --end option.")
                continue
            # Process payeeRules to see if we have a match, otherwise leave blank
            entity = ""
            for rule in payeeRulesTuplesList:
                if re.match(rule[0], line['Description']):
                    entity = rule[1]
                    break
            description = ""
            metadataList = [
                'type: \"' + line['Type'] + '\"',
                'trans-date: \"' + datetime.datetime.strptime(line['Transaction Date'], "%m/%d/%Y").strftime("%Y-%m-%d") + '\"',
                'post-date: \"' + datetime.datetime.strptime(line['Post Date'], "%m/%d/%Y").strftime("%Y-%m-%d") + '\"',
                'statement-desc: \"' + line['Description'] + '\"'
            ]

            # From account will always be Chase Sapphire Reserve
            fromAccount = DEFAULT_ACCOUNT
            toAccount = DEFAULT_CATEGORY
            amount = line['Amount']
            beancountStr = dateStr + " * \"" + entity + "\" \"" + description + "\" #imported\n"
            for metadata in metadataList:
                beancountStr += "  " + metadata + "\n"
            beancountStr += "  " + fromAccount + "    " + str(amount) + " USD\n" + \
                "  " + toAccount + "\n"
            print(beancountStr)
            aggregatedBeancountArray.append(beancountStr)

    # Diff engine
    # Now that we have the CSV file from Chase loaded in with the same structure
    # as beancount, we can read in the beancount file and try to provide a diff
    # to the user. This will only read in beancount data that overlaps the date
    # range contained in the CSV file. (e.g. it maps pretty well to statements!)
    # Determine min/max dates
    #minDate =
    #maxDate =
    #with open('ledger.beancount', 'r') as beancountData:
    #    for

    # Save to beancount data file
    if args.save:
        with open(beancountFile, "a") as fh:
            aggregatedBeancountArray.reverse()
            fh.write("\n")
            for line in aggregatedBeancountArray:
                fh.write(line + "\n")
        print("Results saved to beancount ledger file.")

if __name__ == "__main__":
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(handler)
    _log.setLevel("INFO")
    _log.info("Starting!")
    main()
