#! python3

# Beancount importer that generically provides CSV importing capabilities.
#
# Author: Alex Ford (arf4188@gmail.com)

from argparse import ArgumentParser
import logging
import sys
import csv

logging.basicConfig(level=logging.DEBUG)
_log = logging.getLogger()

def main(args):
    with open(args.input_csv_file) as inCsvFile:
        headerFields = [h.strip() for h in inCsvFile.readline().split(',')]
        csvReader = csv.DictReader(inCsvFile, headerFields)
        for line in csvReader:
            _log.info("{}".format(line))

if __name__ == "__main__":
    _log.info("{} has started.".format(sys.argv[0]))
    parser = ArgumentParser(
        description="Generic CSV importer for Beancount."
    )
    parser.add_argument("input_csv_file",
        help="Path to the CSV file to use as input. The first row should be the header row.")
    parser.add_argument("-o", "--output",
        help="Path to the output file where Beancount transactions will be saved.")
    parser.add_argument("-v", "--verbose",
        action="store_true",
        help="Turns on verbose program output, useful for development and troubleshooting.")
    parser.add_argument("-m", "--map",
        help="Explicitly provide the 'map' to use for this CSV to Beancount import. By default, this will attempt to figure it out automatically.")
    args = parser.parse_args()
    _log.debug("args is: {}".format(args))
    main(args)
    _log.info("{} has finished.".format(sys.argv[0]))
