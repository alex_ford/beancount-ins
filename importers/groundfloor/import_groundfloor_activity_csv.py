#!/usr/bin/env python3

# MATURITY LEVEL: 2/5 (Alpha, usable but limited)

"""This Python script handles the import of Ground Floor's Investment Activity
export CSV which can be obtained by logging into your account, clicking on
"Investment Activity", and then on the button that says "Download Investments".

NOTE: the data contained in this file is NOT a transaction log! It represents
an accounting for each position/loan held, which summarizes a series of transactions.

@author Alex Ford (arf4188@gmail.com)
"""

# TODO: replace with click as it is simplier and easier to use
import argparse
import csv
import arrow
from arrow import Arrow

from beancount_entry    import BeancountEntry
from beancount_metadata import BeancountMetadata
from beancount_posting  import BeancountPosting

# The following is the general structure of the CSV file:
# Transaction Id,Loan,Address 1,Address 2,City,State,Zipcode,Loan Amount,Loan State,Performance State,Rate,Term,Grade,ARV,ARV %,Valuation Type,Security Position,Purpose,Exit Strategy,Purchase Amount,Promtions Amount,Purchased Date,Interest Start Date,Due Date,Repaid Date,Return
# i_a8572a6fc184,811 Acton Avenue,811 Acton Avenue,"",Homewood,AL,35209,"$275,950.00",Funding,Performing,11.0,12,C,"$420,000.00",65.7,Certified Independent Appraisal,Senior,Purchase & Renovation,Sell,$50.00,$0.00,06/22/2018,06/22/2018,06/04/2019,"",""

# The following is the general template of a Ground Floor transaction:
# 2018-05-24 * "Ground Floor" "Invested in XXX"
#   transactionId: ""
#   term: ""
#   rate: ""
#   grade: ""
#   Assets:Investments:GroundFloor:Cash          -0.00 USD
#   Assets:Investments:GroundFloor:Invested

beancountDataFile="..\\beancount-data\\ledger.beancount"
#csvInputFile=".\\import-data\\groundfloor_investments.csv"
csvInputFile="../../../Downloads/groundfloor_investments_10022019-200953.csv"

# Parse CLI arguments
# https://docs.python.org/3/library/argparse.html
parser = argparse.ArgumentParser(description="Imports the Investment Activities CSV downloaded from the Ground Floor website.")
parser.add_argument("--save",
    help="Saves the results to the beancount ledger file. (Default: " + beancountDataFile + ")",
    nargs="?",
    type=str,
    default=beancountDataFile)
parser.add_argument("--use",
    help="Allows you to specify the input CSV file to import. (Default: " + csvInputFile + ")",
    nargs="?",
    type=str,
    default=f"{csvInputFile}")
args = parser.parse_args()

print(args.save)
print(args.use)


def slugify_address(address: str) -> str:
    """Takes a humanized address string and transforms it into a "slug" string, which is
    more computer friendly. This type of procedure is common in URLs, encoding, etc."""
    slug = address.replace(" ", "")
    return slug

def to_address_dict(address: str) -> {}:
    """Takes a humanized address string and breaks it into a dict with each part separated
    into key-values like 'city', 'state', etc."""
    address_dict = {}
    parts = address.split(",") # [0] = city, [1] state zipcode
    address_dict.update({'city': parts[0]})
    parts = parts[1].split(" ") # [0] = state, [1] = zipcode
    address_dict.update({'state': parts[0]})
    address_dict.update({'zipcode': parts[1]})
    return address_dict

bcTxs = []

with open(csvInputFile) as f:
    csvReader = csv.DictReader(f)
    # entries = {
    #   '2019-01-01': [
    #     BeancountEntry,
    #     BeancountEntry,
    #     BeancountEntry
    #   ]
    # }
    entries: dict = {}
    for line in csvReader:
        # safe guard against nullified lines (CSV lines with no data)
        if len(line['Transaction Id']) == 0:
            continue
        #print(line)
        # Transaction Id,Loan,Address 1,Address 2,City,State,Zipcode,Loan Amount,Loan State,Performance State,Rate,Term,Grade,ARV,ARV %,Valuation Type,Security Position,Purpose,Exit Strategy,Purchase Amount,Promtions Amount,Purchased Date,Interest Start Date,Due Date,Repaid Date,Return
        entry = BeancountEntry()
        
        entry.Datetime = arrow.get(line['Purchased Date'], "MM/DD/YYYY")
        entry.Flag = "*"
        entry.Payee = "GroundFloor"
        entry.Description = f"Investment in {line.get('Loan')}"
        entry.Tags = [f"#{slugify_address(line.get('Loan'))}"]
        
        entry.addMetadata(BeancountMetadata("transaction-id", line['Transaction Id']))
        entry.addMetadata(BeancountMetadata("city", line['City']))
        entry.addMetadata(BeancountMetadata("state", line['State']))
        entry.addMetadata(BeancountMetadata("term", f"{line['Term']} months"))
        entry.addMetadata(BeancountMetadata("roi", f"{line['Rate']}%"))
        entry.addMetadata(BeancountMetadata("grade", line['Grade']))
        entry.addMetadata(BeancountMetadata("loan-value", line['Loan Amount'].strip()))
        entry.addMetadata(BeancountMetadata("source", "beancount-ins"))
        
        amount: float = float(line['Purchase Amount'][1:].replace(",",""))
        entry.addPosting(BeancountPosting("Assets:Investments:GroundFloor:Cash", -1*amount))
        entry.addPosting(BeancountPosting("Assets:Investments:GroundFloor:Invested", amount))

        # get the list of entries for the current datetime, or an empty list if nothing was found
        e_list = entries.get(entry.Datetime, [])
        e_list.append(entry)
        entries.update({entry.Datetime: e_list})
        # print(f"{entry}")
        
    # print(f"{entries}")
    for k,v in entries.items():
        print(f"    <==--    {k}    --==>")
        for i in v:
            print(f"{i}")
