#!/usr/env python3

"""
Status: runs and provides interesting output but doesn't do anything to merge
the data with existing beancount data.

Author: Alex Richard Ford (arf4188@gmail.com)
"""


import colorlog
import csv
import arrow

from beancount.core import data

_log = colorlog.getLogger(__name__)

cashAccount = "Assets:Investments:GroundFloor:Cash"
investedAccount = "Assets:Investments:GroundFloor:Invested"

defaultCsvInputPath = "../beancount-data/import-files/inbox/groundfloor_transactions_html2csv.csv"

# after import, the input file will be moved here, placed under a
# "./groundfloor/" directory, and dated with today's date.
defaultProcessedPath = "../beancount-data/import-files/processed/"

def process():
    with open(defaultCsvInputPath) as csvInput:
        csvReader = csv.DictReader(csvInput)
        for row in csvReader:
            _log.info("Row: {}".format(row))
            bcTx = data.Transaction(
                data.new_metadata(None, None, { "transaction-number": l}),
                arrow.get(row['\ufeffDate']),
                "*",
                "GroundFloor",
                row['Description'],
                "Tags",
                "Links",
                [
                    data.Posting(
                        cashAccount,
                        "USD",
                        formatCurrency(row['Amount']),
                        None,
                        None,
                        None
                    ),
                    data.Posting(
                        investedAccount,
                        None,
                        None,
                        None,
                        None,
                        None
                    )
                ]
            )
            print(f"{bcTx.date.format('YYYY-MM-DD')} {bcTx.flag} \"{bcTx.payee}\"  \"{bcTx.narration}\"")
            for p in bcTx.postings:
                if p.cost != None:
                    print(f"  {p.account}        {p.cost} USD")
                else:
                    print(f"  {p.account}")


def formatCurrency(currencyString: str) -> str:
    currencyString = currencyString.replace("$", "")
    if currencyString.startswith("("):
        currencyString = currencyString.replace("(", "-")
        currencyString = currencyString.replace(")", "")
    return currencyString

if __name__ == "__main__":
    _logHandler = colorlog.StreamHandler()
    _logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(_logHandler)
    _log.setLevel("INFO")
    _log.info("Starting!")
    process()
    _log.info("Finished.")
