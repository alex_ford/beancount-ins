#! python3

# MATURITY LEVEL: 2 (Usable/Prototype)

# This python script takes the transactions copy and pasted from the Investor 
# Account Transactions page of the Ground Floor website and parses that into
# cooresponding Beancount transactions.
#
# ** Key Assumptions **
# - Only two accounts are used, Cash and Invested positions, to
#     track Ground Floor activity.
# - Only USD are used.
#
# ** Configuration **
#  1. Update the cashAccount and investedAccount variables near the start of the code.
#
# ** Usage **
#  1. Visit the Ground Floor website in your browser.
#  2. Open the Investor Account Transactions page by clicking
#       "Investor Account" > "Investment Activity"
#  3. Copy the rows you want, NOT included the header, and paste them into a
#       new text file.
#  4. Save the text file into .\import-files\groundfloor-copy-and-pasted-txs.txt then run
#       this script with no arguments.
#  5. Visually verify that the output is acceptable to you and if so, re-run
#       this script with --save <bcdat> to record the tranactions to your
#       Beancount data file.
#
# ** Limitations **
#  TBD
#
# @author Alex Ford (arf4188@gmail.com)

# ** Features/TODO **
# [x] Provides standard argument processing (--help, --save)
# [x] Reads the Ground Floor data copy-and-pasted from Investor Account Transactions page
# [x] Generates cooresponding Beancount transactions
# [] ...

import sys, argparse

from beancount_entry import BeancountEntry
from beancount_metadata import BeancountMetadata
from beancount_posting import BeancountPosting

cashAccount = "Assets:Investments:GroundFloor:Cash"
investedAccount = "Assets:Investments:GroundFloor:Invested"

# Parse CLI arguments
# https://docs.python.org/3/library/argparse.html
parser = argparse.ArgumentParser(
    description="Imports the pasted transactions from the Investor Account "
                "Transactions webpage.")
parser.add_argument("--save",
    help="Saves the results to the beancount ledger file.",
    nargs=1)
# parser.add_argument("--verbose",
#     help="Turns on verbose output which can help in debugging problems.",
#     action="store_true")
# parser.add_argument("--stats",
#     help="Shows processing stats after completion of the import.",
#     action="store_true")
# parser.add_argument("file",
#     help="The CSV file to import, which is from GDAX.")
args = parser.parse_args()

# The copy and pasted data ends up putting out the form:
#
# 05/02/2018	a_a1b28a1b475d	Investment In 205 West 5th Avenue	
# ($40.00)
# $7.38
#
# Which can be generalized as:
#
# [date] [txid] [description]
# [amount]
# [balance]
#
# We don't need the [balance] part, so we can ignore/discard that
# TODO provide option to generate balance assertions using the [balance]
# data from above? e.g. if user passes in --assert-balance then this can
# generate Beancount balance assertions.

entries = []
# TODO allow this to be specified via script argument
with open("import-files/groundfloor-copy-and-pasted-txs.txt") as f:
    
    lines = f.readlines()
    i = 0
    while i < len(lines):
        beancountEntry = BeancountEntry()
        parts = lines[i].split("\t")
        parts = [x.strip() for x in parts ]
        #print(parts)
        beancountEntry.setDate(parts[0])
        beancountEntry.Payee = "Ground Floor"
        beancountEntry.Description = parts[2]
        beancountEntry.addMetadata(
            BeancountMetadata(key="txid", value=parts[1]))
        #print(lines[i+1])
        # Fetch the amount next, which may come in as $40.00 or ($40.00)
        amt = lines[i+1].split()[0]
        if amt.startswith("("):
            amt = amt.replace("(", "-").replace(")", "")
        # We just strip the $ off for now...
        amt = amt.replace("$", "")
        amt = float(amt)
        beancountEntry.addPosting(
            BeancountPosting(account=cashAccount, amountValue=amt))
        beancountEntry.addPosting(
            BeancountPosting(account=investedAccount, amountValue=-amt))
        #print(lines[i+2])
        print(beancountEntry)
        entries.append(beancountEntry)
        i = i + 3

if args.save:
    with open(args.save[0], "a") as fh:
        #aggregatedBeancountArray.reverse()
        for entry in entries:
            fh.write(entry.toBeancountStr() + "\n")
    print("Results saved to beancount ledger file: " + args.save[0])
