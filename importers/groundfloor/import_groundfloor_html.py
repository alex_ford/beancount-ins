#!/usr/env python3

# MATURITY LEVEL: 0 (IDEATION)

"""
This python script takes the saved HTML from the GroundFloor website, from the
Investment Activity > Transactions page, and attempts to process it into
Beancount transactions.

This is needed because GroundFloor doesn't seem to offer a way to download this
data directly as a more consumable format, like CSV. This script in particular
focuses on taking the HTML and producing an interim CSV that can be fed into the
./import_groundfloor_transactions_csv.py script.

Key Assumptions
- Only two accounts are used, Cash and Invested positions, to
    track Ground Floor activity.
- Only USD are used.
- Hashtags are used to mark all transactions belonging to a particular investment.
- Hashtags are a short-hand form of the investment address.

Usage
```shell
cd ./beancount-ins/
pipenv install
pipenv run python ./importers/ground-floor/import_groundfloor_html.py /path/to/inputFile
```

Author: Alex Ford (arf4188@gmail.com)
"""

# Features/TODO
# [] Provides standard argument processing (--help, --save)
# [] Reads the Ground Floor HTML (produced via 'save html')
# [] Extracts the date, amount, and description from the HTML
# [] Extracts metadata from the HTML (txid, time, term-months)
# [] ...

import logging
import colorlog
from html.parser import HTMLParser
from lxml import html

_log = colorlog.getLogger(__name__)
handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter())
_log.addHandler(handler)

cashAccount = "Assets:Investments:GroundFloor:Cash"
investedAccount = "Assets:Investments:GroundFloor:Invested"

htmlFile = "../beancount-data/import-files/inbox/groundfloor_transactions.html"

def main():
    with open(htmlFile, "r") as inputFile:
        tree = html.parse(inputFile)
        _log.info(tree)
        _log.info("{}".format(tree.xpath("//table")[0].text_content()))
        # parser = HTMLParser()
        # for inputLine in inputFile:
        #     parser.feed(inputLine)
        # parser.close()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
    _log.warn("More coming soon!")
