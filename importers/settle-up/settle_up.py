#!/usr/env python3

"""Tailored interface between Beancount and the Settle Up expense sharing app.

Settle Up is a group expense sharing application for web and mobile.

General Website:
    https://www.settleup.io

API Documentation:
    https://docs.google.com/document/d/18mxnyYSm39cbceA2FxFLiOfyyanaBY6ogG7oscgghxU/edit#heading=h.c38yf4mz8bod

Author: Alex Richard Ford (arf4188@gmail.com)
"""

@staticmethod
def createSandboxConnection() -> SettleUpAPI:
    url: str = "https://settle-up-sandbox.firebaseio.com"
    web_api_key: str = "AIzaSyCfMEZut1bOgu9d1NHrJiZ7ruRdzfKEHbk"
    google_client_id: str = "84712828597-cbvr8ovmj77r736kegrjuoq6697e8m68.apps.googleusercontent.com"
    facebook_app_id: str = "327675517252504"
    return None

@staticmethod
def createProdConnection() -> SettleUpAPI:
    url: str = "https://settle-up-live.firebaseio.com"
    throw RuntimeError("Not impelmented yet!")

class SettleUpAPI()

    def __init__(self):
        pass
