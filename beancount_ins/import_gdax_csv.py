# For taking GDAX CSV files, obtained by looking at your individual accounts
# on the website and then exporting the transaction history to CSV, and turning
# them into Beancount entries.

import csv, datetime, argparse

# The CSV headers are: (there are others, but these are the important ones)
# type,	time,	amount,		balance,	amount/balance unit

# if type = 'deposit' then we use 'Transfer' for the payee, otherwise we use 'GDAX'

importStartTime = datetime.datetime.now()
csvEntries = 0
genBcEntries = 0

# Setup program arguments
# https://docs.python.org/3/library/argparse.html
parser = argparse.ArgumentParser();
parser.add_argument("--save",
    help="Saves the results to the beancount ledger file.",
    action="store_true")
parser.add_argument("--verbose",
    help="Turns on verbose output which can help in debugging problems.",
    action="store_true")
parser.add_argument("--stats",
    help="Shows processing stats after completion of the import.",
    action="store_true")
parser.add_argument("file",
    help="The CSV file to import, which is from GDAX.")

args = parser.parse_args()
file = args.file
verbose = args.verbose

aggregatedBeancountArray = []
with open(file, 'r') as csvFile:
    csvDictReader = csv.DictReader(csvFile)
    for csvLine in csvDictReader:
        csvEntries += 1
        if verbose:
            print(csvLine)

        txType = csvLine['type']

        # This actually tells us which account all the txs are from
        accountUnit = csvLine['amount/balance unit']

        # Parse the time out of the GDAX CSV file; we can safely assume UTC/Zulu time always
        # http://strftime.org/
        dateObj = datetime.datetime.strptime(csvLine['time'], "%Y-%m-%dT%H:%M:%S.%fZ")
        dateStr = dateObj.strftime("%Y-%m-%d")
        timeStr = dateObj.strftime("%H:%M:%S UTC")

        # When using import script, we should always use the 'needs attention'
        # flag instead of the 'reconciled' flag
        flag = "!"

        # Default value, this should get replaced in the code that follows
        payee = "DEFAULT PAYEE"

        # Default value, this should get replaced in the code that follows
        desc = "DEFAULT DESCRIPTION"

        if accountUnit == "USD":
            posting1Account = "Assets:Investments:GDAX:Cash"
        elif accountUnit == "BTC":
            posting1Account = "Assets:Investments:GDAX:Bitcoin"
        elif accountUnit == "BCH":
            posting1Account = "Assets:Investments:GDAX:Bitcoin-Cash"
        elif accountUnit == "ETH":
            posting1Account = "Assets:Investments:GDAX:Ethereum"
        elif accountUnit == "LTC":
            posting1Account = "Assets:Investments:GDAX:Litecoin"
        else:
            raise Exception("Could not determine account")
        posting1Amount = float(csvLine['amount'])
        posting1Unit = accountUnit

        # Determine the cost basis/lot spec
        # This is not relevant if the accountUnit is "USD"
        posting1Cost = ""
        # TODO
        if accountUnit == "BTC":
            posting1Cost = "{ ??? USD, " + dateStr + " }"

        posting2Account = "Equity:Opening-Balances"

        # Other metadata fields
        metaTransferId = csvLine['transfer id']
        metaTradeId = csvLine['trade id']
        metaOrderId = csvLine['order id']

        # This is the meat of how we handle each transaction in the CSV file based
        # on what it's declared type is
        if txType == "deposit":
            payee = "Transfer"
            desc = "Transfer " + accountUnit + " from ??? to GDAX"
        elif txType == "withdrawal":
            payee = "Transfer"
            desc = "Transfer " + accountUnit + " from GDAX to ???"
        elif txType == "match":
            payee = "GDAX"
            # if posting1Amount is negative, then it means we sold USD and bought some crypto
            # And vice versa!
            if posting1Amount < 0:
                desc = "Bought ???"
            else:
                desc = "Sold ???"
        elif txType == "fee":
            # We want to combine this with the previous entry
            lastBcEntry = aggregatedBeancountArray.pop()
            lastBcEntry += "  " + posting1Account + "   " + "{0:.8f}".format(posting1Amount) + " " + posting1Unit + " ; fee\n"
            aggregatedBeancountArray.append(lastBcEntry)
            print(aggregatedBeancountArray[len(aggregatedBeancountArray)-1])
            continue
        else:
            raise Exception("Unhandled transaction type!")

        # Format beancount entry, print, and add to array
        bcEntry = dateStr + " " + flag + " \"" + payee + "\" \"" + desc + "\"\n"
        # Add the metadata fields
        bcEntry += "  time: \"" + timeStr + "\"\n"
        if metaTransferId:
            bcEntry += "  transfer-id: \"" + metaTransferId + "\"\n"
        if metaTradeId:
            bcEntry += "  trade-id: \"" + metaTradeId + "\"\n"
        if metaOrderId:
            bcEntry += "  order-id: \"" + metaOrderId + "\"\n"
        # Add the postings
        bcEntry += "  " + posting1Account + "   " + "{0:.8f}".format(posting1Amount) + " " + posting1Unit + " " + posting1Cost + "\n"
        bcEntry += "  " + posting2Account + "\n"
        print(bcEntry)
        aggregatedBeancountArray.append(bcEntry)
        genBcEntries += 1

# Display stats if requested
if args.stats:
    print("Statistics for import session")
    print("\t\t\tImport took: \t" + str(datetime.datetime.now() - importStartTime)[0:-3])
    print("\t\t\tCSV entries: \t" + str(csvEntries))
    print("\tGenerated beancount entries: \t" + str(genBcEntries))
