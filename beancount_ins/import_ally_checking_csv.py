#! python3

# MATURITY LEVEL: 0
# For reference, the options for the above are:
#   0 (Ideation)
#   1 (Unit Prototype)
#   2 (Functional Prototype)
#   3 (Alpha Ready)
#   4 (Alpha-2 Ready)
#   5 (Beta Ready)
#   6 (Beta-2 Ready)
#   7 (Generally Ready)

import csv
import datetime
import argparse
import re
import os
import logging # TODO: switch out for colorlog

#from beancount_arfsrc.beancount_entry import BeancountEntry
# from beancount import core.dir
# from beancount.core import account


# The Ally Bank CSV importer. Simply log into your Ally Bank account and download the CSV
# transactions for your account. Place them into your beancount-data\import-files\ directory
# as "ally-checking.csv" and run this script.
#
# Example:
#       pipenv run python import-ally-csv.py ../beancount-data/import-files/inbox/ally-checking.csv
#
# @author Alex Richard Ford (arf4188@gmail.com)
# @website http://www.alexrichardford.com
# @license MIT License

# The DEFAULT_CATEGORY is used as the default account for the 2nd posting of a Beancount transaction.
DEFAULT_CATEGORY = "! Expenses:Other"
# The DEFAULT_ACCOUNT is used as the account for the 1st posting of the Beancount transaction. This
# should basically represent the account that this importer is responsible for.
DEFAULT_ACCOUNT = "Assets:Banking:Ally:Checking"
# The default location of the beancount data file.
beancountFile = os.path.expanduser("~/Data/Personal/beancount-data/main.beancount")
# The default location of the payeeRulesFile which can provide better payee values
payeeRulesFile = os.path.expanduser("~/Data/Personal/beancount-data/configuration/ins-payee-rules.bcspec")

##################
# Logging Controls
##################

# Controls the logging level sent to the console. If experiencing trouble, it may be useful
# to change this to show logging.DEBUG level logs.
# logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.INFO)

# Enable the following if you would like to log to file.
#logging.basicConfig(filename='example.log',level=logging.DEBUG)

##################
# Program begins
##################
parser = argparse.ArgumentParser(description="Allows you to import Beancount transactions from a source CSV file exported from the Ally Banking website.")
parser.add_argument("csvFile",
    help="The CSV file downloaded from the Ally.com website with the transactions you wish to import into Beancount format.",
    type=argparse.FileType(mode='r', encoding='UTF8'))
parser.add_argument("-s", "--save",
    help="Saves the results to the beancount ledger file.",
    action="store_true")
parser.add_argument("--start",
    help="Specify a starting date to bound the data imported from the input file. Use YYYY-MM-DD format.",
    default=None)
parser.add_argument("--end",
    help="Specify a starting date to bound the data imported from the input file. Use YYYY-MM-DD format.",
    default=None)
args = parser.parse_args()

logging.info("Using input file: {}".format(args.csvFile))

if args.start != None:
    # Validation check
    if re.match(r"\d{4}-\d{2}-\d{2}", args.start):
        logging.info("Using start date of: " + args.start)
    else:
        logging.error("Invalid format for --start option value.")
        exit(-1)
if args.end != None:
    # Validation check
    if re.match(r"\d{4}-\d{2}-\d{2}", args.end):
        logging.info("Using end date of: " + args.end)
    else:
        logging.error("Invalid format for --end option value.")
        exit(-1)

# Load up the Rules files
payeeRulesTuplesList = []
with open(payeeRulesFile, "r") as prf:
    for line in prf:
        if line.startswith("#") or line.startswith("\n"):
            continue
        tokens = line.split("=")
        payeeRulesTuplesList.append((tokens[0].strip(), tokens[1].strip()))
logging.info("{}".format(payeeRulesTuplesList))

# Basic beancount transactions look like this:
#     2017-09-14 * "Chase" "CSR CC payment"
#         Assets:Banking:Ally:Checking                                               -3204.00 USD
#         Liabilities:Credit-Cards:Chase-Sapphire-Reserve
skipOld = 0
skipNoAccount = 0
skippedMintAccounts = set()
aggregatedBeancountArray = []
# Ally uses the following CSV headers
# Date, Time, Amount, Type, Description
with args.csvFile as csvFile:
    # Read the header line explicitly so we can ensure it's presented correctly
    # This removes any leading/trailing spaces form the keynames which can cause
    # problems for the CSV DictReader.
    header = [h.strip() for h in csvFile.readline().split(',')]
    reader = csv.DictReader(csvFile, fieldnames=header)
    logging.debug("CSV keys: {}".format(reader.fieldnames))
    for line in reader:
        dateObj = datetime.datetime.strptime(line['Date'], "%Y-%m-%d")
        dateStr = dateObj.strftime("%Y-%m-%d")
        # Check against start/end if needed
        if args.start != None and datetime.datetime.strptime(args.start, "%Y-%m-%d") > dateObj:
            logging.debug("Skipping transaction from " + dateStr + " since it comes before --start option.")
            continue
        if args.end != None and datetime.datetime.strptime(args.end, "%Y-%m-%d") < dateObj:
            logging.debug("Skipping transaction from " + dateStr + " since it comes after --end option.")
            continue
        # Process payeeRules to see if we have a match, otherwise leave blank
        entity = ""
        for rule in payeeRulesTuplesList:
            if re.match(rule[0], line['Description']):
                entity = rule[1]
                break
        # Currently not sophisticated enough to come up with an automatic description
        description = ""
        # Fill in the various metadata properties
        metadataList = [
            'type: \"' + line['Type'] + '\"',
            'post-time: \"' + line['Time'] + ' EST\"',
            'statement-desc: \"' + line['Description'] + '\"'
        ]

        # From account will always be DEFAULT_ACCOUNT
        fromAccount = DEFAULT_ACCOUNT
        toAccount = DEFAULT_CATEGORY
        amount = line['Amount']

        # Fill out a Beancount Entry object

        beancountStr = dateStr + " * \"" + entity + "\" \"" + description + "\" #imported\n"
        for metadata in metadataList:
            beancountStr += "  " + metadata + "\n"
        beancountStr += "  " + fromAccount + "    " + str(amount) + " USD\n" + \
            "  " + toAccount + "\n"
        print(beancountStr)
        aggregatedBeancountArray.append(beancountStr)

# Diff engine
# Now that we have the CSV file from Chase loaded in with the same structure
# as beancount, we can read in the beancount file and try to provide a diff
# to the user. This will only read in beancount data that overlaps the date
# range contained in the CSV file. (e.g. it maps pretty well to statements!)
# Determine min/max dates
#minDate =
#maxDate =
#with open('ledger.beancount', 'r') as beancountData:
#    for

# Save to beancount data file
if args.save:
    with open(beancountFile, "a") as fh:
        aggregatedBeancountArray.reverse()
        fh.write("\n")
        for line in aggregatedBeancountArray:
            fh.write(line + "\n")
    print("Results saved to beancount ledger file.")
