#!/usr/bin/env python3

"""Diff takes two Beancount data files and attempts to "diff" them.

Usage: python .\diff.py src1 src2

The 'src1' file should be your actual beancount data file.
The 'src2' file should be a (presumably, though not necessarily) smaller
beancount data file that you most likely intend on merging into the source file.
Purpose of this script

A lot of my import scripts essentially take in one format and spit out
beancount data. You can write this output to a temporary file and then use
this diff script to manually reconcile the differences. This process is way
faster than manually attempting to find the transactions in your file one by one.

# Matching

Any two transactions in 'source' and 'incoming' match if their dates are the
same. This may be improved on in the future.

# FEATURES

[ ] Reads in source and incoming beancount data files
[ ] Provides 'inline' diff view between the files
[ ] Allows for assisted handling of differences (merge, accept, skip, etc.)

Author: Alex Richard Ford (arf4188@gmail.com)
"""

import argparse

argParser = argparse.ArgumentParser(description="Shows the difference between the 'source' and 'incoming' beancount data files.code ")
argParser.add_argument("source",    
    help="The source beancount data file, typically your main ledger file.")
argParser.add_argument("incoming",
    help="The incoming beancount data file, most likely created from running one of the import scripts.")
args = argParser.parse_args()

print("Source: " + args.source)
print("Incoming: " + args.incoming)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

with open(args.source) as src:
    # setup and initial validation work
    # ...
    with open(args.incoming) as inc:
        # setup and validate

        # get the earliest and latest dates from incoming file
        # used to limit what we store from the source file

        # read in source file, keep only what we need

        # read in incoming file

        # output diff based on transaction date in sorted order
        # example:
        #  source                  |   incoming
        #  2018-06-01 * ...        | 2018-06-01 * ...
        #    ...                   |   ...
        #                         < > 2018-06-02 * ...
        #                          |   ...
        #  2018-06-03 * ...        | 2018-06-03 * ...
        #    ...                   |   ...
        # Initial version will produce a side-by-side comparison output in plain text
        # with the txs sorted by date on each side. Where one file contains a transaction 
        # but the other does not, empty space will be displayed along with the center column
        # changed to the "< >" indicator.
        # Limitations:
        #   - this doesn't look at anything other than date for right now, so this means
        #       txs with the same date will be placed side-by-side but might actually not
        #       be a matching transaction at all.
        pass
