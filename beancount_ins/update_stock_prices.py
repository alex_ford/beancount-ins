#!/usr/bin/python

"""Fetch stock prices from an Internet data source for a given set of stocks.

    This uses Alpha Vantage API key, obtained free from: https://www.alphavantage.co
    API Documentation: https://www.alphavantage.co/documentation/

    Sample Usage:

        Fetch updated quotes for the stocks, printing them to the terminal, then exiting:
            pipenv run update-stock-prices

        Same as above, but save the results to the specified Beancount file:
            pipenv run update-stock-prices -s ~/Data/Personal/beancount-data/other-data/prices.beancount

    Author:  Alex Richard Westhaver-Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
"""

import os
import sys
import colorlog
import requests
import datetime
import argparse
import arrow

_log = colorlog.getLogger(
    f"beancount_ins.{os.path.basename(__file__)}.{__name__}")

# TODO: [ ] Support for different stock exchange prefix in creation of output.
# TODO: [ ] Use the Real Beancount Library

# Load private API Key from user's beancount-data directory
# TODO: get path to API key from bean-config
source_api_key_file: str = "~/Data/Personal/beancount-data/configuration/apis/alpha-vantage-api.key"
source_api_key_file = os.path.expanduser(source_api_key_file)
api_key = None
with open(source_api_key_file, "r") as api_key_file:
    api_key = api_key_file.readline().strip()
    _log.info(f"Successfully loaded API key file!")
if api_key is None:
    raise RuntimeError(f"Could not load Alpha Vantage API Key from secrets file! Ensure that it exists at {source_api_key_file} before proceeding. If you need a free API key, you can visit their website at https://alphavantage.com")

# https://www.alphavantage.co/documentation/#daily
# TODO: use urllib to construct the URL instead of doing it like this
URL = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=K&apikey={api_key}"

# The list of stocks to fetch updates for. Add/remove from this as needed.
# These should match the NYSE ticker symbol!
STOCKS = [
    #"APTO",
    #"GRPN",
    #"HDP",
    "JBLU",
    "K",
    "S",
    "W"
]


def main():
    """Main code usable from the user's command line."""
    # Setup basic logger configuration when running in "main mode"
    logHandler = colorlog.StreamHandler()
    logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(logHandler)
    _log.setLevel("INFO")
    _log.info("Update Stock Prices has started...")

    # Program begins
    args_parser = argparse.ArgumentParser(
        description="Updates the stock prices for each stock in the specified "
        "list. The default behavior of this utility is to simply output "
        "the results to the console without writing anything to file. See "
        "the --save argument for how to write to a specific file."
    )
    args_parser.add_argument("-s", "--save",
                             dest="save_file",
                             help="Saves the results to the specified Beancount data file."
                             )
    args_parser.add_argument("--debug",
                             action="store_true",
                             help="Enables debug logging for help troubleshooting this utility."
                             )
    # TODO: add feature --list to show the list of stocks that would be fetched

    args = args_parser.parse_args()
    if args.debug:
        _log.setLevel("DEBUG")
        _log.debug("DEBUG OUTPUT ENABLED!")

    _log.debug(f"args = {args}")

    # Quick file sanity check
    if args.save_file and not os.access(args.save_file, os.W_OK):
        _log.error(
            "Either the file does not exist, or is not writable by this script.")
        exit(1)

    # Change query_time to any valid python datetime object in order to change the
    # date when prices are fetched.
    # TODO: switch out in favor of Arrow module
    query_time = datetime.datetime.now()

    # Use proper Beancount data model classes for this
    beancount_string = ""
    _log.info(f"Stock quotes request for: {STOCKS}")
    from alpha_vantage.timeseries import TimeSeries

    for stock in STOCKS:
        # TODO: don't set the key like this!
        ts = TimeSeries(key='QEITGUNZN3HQ3V9X', rapidapi=False)
        data, meta_data = ts.get_intraday(stock)

        # data is a large JSON object mapping datetimes to quote data
        #_log.info(f"data::raw => {data}")

        quote = next(iter(data.items()))
        #_log.info(f"data => {quote}")
        ###  ('2021-09-09 16:15:00', {'1. open': '265.8900', '2. high': '265.8900', '3. low': '265.8900', '4. close': '265.8900', '5. volume': '6405'})
    
        source_string = "alphavantage.co"
        symbol_string = stock
        quote_timestamp = quote[0]
        quote_timestamp_arrow = arrow.get(quote_timestamp)
        quote_date_str = quote_timestamp_arrow.format('YYYY-MM-DD')
        quote_time_str = quote_timestamp_arrow.format('HH:mm:ss ZZ')
        quote_close_value = quote[1]['4. close']
        _log.info(f"{symbol_string} => {quote_timestamp} => close: {quote_close_value} USD")
    
        #_log.info(f"meta_data => {meta_data}")

        # Build the Beancount Price Directive
        beancount_string += quote_date_str + " price NYSE-" + \
            symbol_string + "    " + quote_close_value + " USD\n"
        beancount_string += "  time: \"" + quote_timestamp + " EST\"\n"
        beancount_string += "  source: \"" + source_string + "\"\n"
        _log.info(f"Beancount Price Directive => \n{beancount_string}")

    # Save the Directive(s) to the user's price file
    if args.save_file:
        with open(args.save_file, "a") as fh:
            fh.write("\n")
            fh.write(beancount_string + "\n")
            _log.info("Results saved to Beancount data file: " + args.save_file)
    exit(0)
#    response = requests.get("https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=" +
 #                           ",".join(STOCKS) + "&apikey=QEITGUNZN3HQ3V9X&outputsize=compact")
#    _log.debug(f"response.status_code => {response.status_code}")
#    if response.status_code != 200:
#        _log.error("An error has occurred!")
#        _log.error(f"{resp_json}")
#        exit(1)
#    if response.json()['Error Message'] != None:
#        _log.error(f"The following error was received from the API:\n" +
#                   f"\t{response.json()['Error Message']}")
#        exit(1)

    source_string = "alphavantage.co"
    date_string = "{0:%Y-%m-%d}".format(query_time)
    time_string = "{0:%H:%M:%S}".format(query_time)
    _log.debug(f"resp_json => {resp_json}")
    try:
        stock_quotes = resp_json['Stock Quotes']
        for sq in stock_quotes:
            # TODO: use f-strings to build up the strings instead of operating concatenation
            symbol_string = sq["1. symbol"]
            price_string = sq["2. price"]
            timestamp_string = sq["4. timestamp"]
            beancount_string += date_string + " price NYSE-" + \
                symbol_string + "    " + price_string + " USD\n"
            beancount_string += "  time: \"" + timestamp_string + " EST\"\n"
            beancount_string += "  source: \"" + source_string + "\"\n"
    except KeyError as ke:
        _log.exception(ke)
        _log.error("Perhaps we hit the freebie API limit?")

    # Use normal print() method instead of _log so we can pipe output to beancount file
    print(" ---- RESULT OUTPUT ----")
    print("")
    print(beancount_string)
    print("")

    if args.save_file:
        with open(args.save_file, "a") as fh:
            fh.write("\n")
            fh.write(beancount_string + "\n")
            _log.info("Results saved to Beancount data file: " + args.save_file)


if __name__ == "__main__":
    main()
