#!/usr/bin/env python3

"""Import WavesDEX CSV transaction file.

This python script simply takes in the WavesDEX CSV transaction
file and produces beancount entries for them!

Author: Alex Ford (arf4188@gmail.com)
"""

# Basic Usage
# 1. Open Waves Client desktop app
# 2. Press the Export button on the Transactions tab
# 3. Save the file as ../beancount-data/import-files/waves-transactions.csv
# 4. Run this script: python import-wavesdex-csv.py
# 5. Reconcile with beancount data file

# Features/Todos
# [x] Read in CSV file
# [x] Create Beancount entries
# [x] Token name to Beancount Symbol Map
#       Sometimes we can just use the 'asset ticket' in the CSV file, but
#       this isn't always provided, and we may want to map it to a different
#       symbol in beancount anyway.
# [x] Imports address data as metadata field(s)
# [] Creates a reasonable description from WavesDEX CSV data
# [] Intelligent auto matching of 2nd posting (classification)
# [] Filter by date range specified as command line arguments (--since 2018-04-22)
# [] Match and deduplicate existing entries in bc file
# [] Use Beancount objects and reusable code
# [] Supports import of fee tx data
# [] Auto handling of lot spec from beancount file
#       Script can read in bc file and compute the available lots for
#       a given date, and then apply a method (FIFO, LIFO, etc.) to
#       auto assign the lots. This should be an optional feature, as it
#       has a high change of not working in many scenarios.
# [] Auto creation of new WaveDEX tokens
#       Waves tokens can be unexpectedly deposited into my address(es). It
#       would be neat if this script could know which ones I've already cataloged
#       and then create new ones when they appear.

import csv, datetime, argparse

# Default location of main beancount data file
beancountFile = "../beancount-data/main.beancount"

# Program begins
parser = argparse.ArgumentParser()
parser.add_argument("--save",  
    help="Saves the results to the beancount ledger file.",
    action="store_true")
parser.add_argument("--file", 
    help="Uses the specified file as the input to this utility.",
    default="../beancount-data/import-files/wavesdex-transactions.csv")
args = parser.parse_args()

print(args.file)

# For reference, beancount transactions, with lot specs,
# look like this:
# 2018-04-15 ! "Payee" "Description"
#   time: "00:00 EST"
#   txid: "value"
#   txSender: "3PQiyYpbPGcyURNbKKkiteQNST8RKSuaWyY"
#   txRecipient: "3P6kKwBR7hMxy4JRhENzmasMWp4DFAFKubq"
#   Assets:Investments:WavesDEX:Litecoin -1.00 LTC { 0.00 USD, 2018-04-15 }
#   Assets:Investments:WavesDEX:Waves 32.00 WAVES { 0.00 USD, 2018-04-15 }

with open(args.file, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for line in reader:
        # See https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
        dateObj = datetime.datetime.strptime(line['Date'], "%d.%m.%Y %H:%M:%S")
        dateStr = dateObj.strftime("%Y-%m-%d")
        timeStr = dateObj.strftime("%H:%M:%S")
        txidStr = line['Transaction ID']

        flag = "*"
        entity = "Unknown"
        description = "None"
        account1 = "Assets:Investments:WavesDEX"
        amount = line['Amount asset value']
        amountSymbol = line['Amount asset ticker']
        account2 = "None"

        # Handle mapping of the subaccount and symbol from the CSV to my beancount file
        if amountSymbol == "MRT":
            account1 += ":Other-Tokens:Miner-Reward"
            amountSymbol = "WAV-MRT"
        elif amountSymbol == "LTC":
            account1 += ":Litecoin"
        elif amountSymbol == "WAVES":
            account1 += ":Waves"
            amountSymbol = "WAV"
        elif amountSymbol == "WFN":
            account1 += ":Other-Tokens:WavesFullNode"
            amountSymbol = "WAV-WFN"
        elif amountSymbol == "LIQUID":
            account1 += ":Other-Tokens:Liquid"
            amountSymbol = "WAV-LIQ"
        
        txSender = line['Transaction sender']
        txRecipient = line['Transaction recipient']

        # Build the beancount transaction string
        beancountStr = dateStr + " " + flag + " \"" + entity + "\" \"" + description + "\"\n" + \
            "  " + "time: \"" + timeStr + "\"\n" + \
            "  " + "txid: \"" + txidStr + "\"\n" + \
            "  " + "txSender: \"" + txSender + "\"\n" + \
            "  " + "txRecipient: \"" + txRecipient + "\"\n" + \
            "  " + account1 + "    " + str(amount) + " " + amountSymbol + "\n" + \
            "  " + account2 + "\n"
        
        print(beancountStr)
