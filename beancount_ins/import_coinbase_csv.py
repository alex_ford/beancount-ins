# For taking Coinbase CSV files, as obtained from the "export" function
# on the Coinbase website, and turning them into Beancount transactions.

import csv, datetime, argparse

# Coinbase CSV files have 5 lines of header data, before transaction data
# actually begins.

coinbaseCsvFile = "C:/Users/arf41/Downloads/Coinbase-58aa47062fb0fa18e9714bf0-Transactions-Report-2018-02-27-01_32_01.csv"

aggregatedBeancountArray = []
with open(coinbaseCsvFile, 'r') as csvfile:
    for i in range(4): csvfile.readline()
    reader = csv.DictReader(csvfile)
    for line in reader:
        print(line)
        print()

        dateObj = datetime.datetime.strptime(line['Timestamp'], "%Y-%m-%d %H:%M:%S %z")
        dateStr = dateObj.strftime("%Y-%m-%d")

        entity = "Coinbase"
        description = line['Notes']

        # From account will always be Chase Sapphire Reserve
        fromAccount = "???" #"Liabilities:Credit-Cards:Chase-Sapphire-Reserve"
        toAccount = "???" #DEFAULT_CATEGORY
        amount = line['Amount']
        amountCurrency = line['Currency']
        amount2 = line['Transfer Total']
        beancountStr = dateStr + " * \"" + entity + "\" \"" + description + "\"\n" + \
             "  " + fromAccount + "    " + str(amount) + " " + amountCurrency + " { ?, " + dateStr + " }\n" + \
             "  " + toAccount + "    " + str((float(line['Transfer Total']) + float(line['Transfer Fee']))) + "\n"
        print(beancountStr)
        aggregatedBeancountArray.append(beancountStr)
        exit()
