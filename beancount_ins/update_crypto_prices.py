#!/usr/bin/env python3

"""Fetch updated cryptocurrency prices from the Internet.

Fetching from the supported data source provider.
This script uses [CoinMarketCap](https://coinmarketcap.com) to fetch
cryptocurrency price data. The data provided is an aggregated JSON response
from a single REST endpoint for all of the cryptos we are querying for.

CoinMarketCap (CMC) has a few different service levels/tiers, with the Free/Basic
level having the following restrictions:
* 5 market data endpoints
* 10K call credits/mo
* No historical data
* Personal use
https://coinmarketcap.com/api/pricing/

In order to use the CoinMarketCap API, you must acquire your own API key, which
is free as described above... or paid, if you choose to do that. A new API key
can be created here: https://pro.coinmarketcap.com/signup/?plan=0
Place the key into a file at the root of this project/Git repo in a file with
the name: ./'CoinMarketCapAPIKey'

https://coinmarketcap.com/api/documentation/v1/
https://pro.coinmarketcap.com/account/

# Endpoint

This only needs the /v1/cryptocurrency/quotes/latest endpoint, documentation
can be found here:
https://coinmarketcap.com/api/documentation/v1/#operation/getV1CryptocurrencyQuotesLatest
The only query params used is the 'symbol' one. Here is a pre-built sample URL
that returns the latest prices for Bitcoin and Ethereum. Change "{REPLACE_ME}"
with your API key.
https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?CMC_PRO_API_KEY={REPLACE_ME}&symbol=BTC,ETH

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License (see LICENSE file)
"""

# TODO: [ ] Gracefully fail on network error (i.e. don't modify the data file)
# TODO: [ ] Format the spaces so that the value amounts are aligned with Fava's formatter
# TODO: [ ] Ability to fetch historical price data when given date as arg to this script
# TODO: [ ] Insert entries into data file at the correct date position
# TODO: [ ] Provide sorting options over the result

import colorlog
import sys
import requests
from arrow import Arrow
import argparse
import os
import json

_VERSION = "0.6.0"
_log = colorlog.getLogger(
    f"beancount_ins.{os.path.basename(__file__)}.{__name__}")

# Change query_time to any valid python datetime object
# in order to change the date when prices are fetched.
query_time: Arrow = Arrow.now()

# Name of the price source
source_name: str = "coinmarketcap.com"
#source_url: str = "https://api.coinmarketcap.com/v1/ticker/?limit=0"

# Base source_url to the endpoint that provides the data we want.
# These query parameters should be supplied:
#   CMC_PRO_API_KEY=
#   symbol=
source_url: str = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest"

# TODO: import and use bean-config to manage this path
source_api_key_file: str = "~/Data/Personal/beancount-data/configuration/apis/coinmarketcap-api.key"
source_api_key_file = os.path.expanduser(source_api_key_file)

# Dict of cryptocurrencies to fetch price data for.
# The keys should be what is declared on coinmarketcap.com as the "symbol",
# and the value is what we want to record it as in own beancount books.
# Add/remove as needed.
# Punch in https://api.coinmarketcap.com/v1/ticker/?limit=0 into a browser
# and find the crypto symbol desired.
coinsMap = {
    'BTC': 'BTC',
    'BCH': 'BCH',
    'ETH': 'ETH',
    'LTC': 'LTC',
    'XMR': 'XMR',
    'XRP': 'XRP',
    'SC': 'SC',
    'MIOTA': "MI-IOTA",
    'WAVES': 'WAV',
    'BTG': 'BTG',
    'DASH': 'DASH',
    'BTX': 'BTX',
    'STEEM': 'STEEM',
    'XLM': 'XLM',
    'BCA': 'BCA',
    'ADA': 'ADA',
    'BNB': 'BNB',
    'DATA': 'ERC20-DATA',
    'OMG': 'ERC20-OMG',
    # 'XNN': 'ERC20-XNN',
    'BCD': 'BCD',
    'BTCP': 'BTCP',
    'BCPT': 'BCPT',
}


def main():
    """Main method for use by users on the command line."""
    streamHdlr = colorlog.StreamHandler()
    streamHdlr.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(streamHdlr)
    _log.setLevel("INFO")

    args_parser = argparse.ArgumentParser(
        description="Updates the list of cryptocurrency prices and generates "
                    "Beancount price entries for each. The default behavior is "
                    "to print the results to stdout, but the --save argument "
                    "can be used to also save the prices in Beancount format "
                    "to a specified file."
    )
    args_parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s v{_VERSION}"
    )
    args_parser.add_argument(
        "--debug", "-v", "--verbose",
        action="store_true",
        help="Enable extra logging output from this script."
    )
    args_parser.add_argument(
        "-s", "--save",
        nargs="?",
        dest="output_file",
        type=argparse.FileType(mode='a'),
        help="Optionally specify the file to append the Beancount data to.",
        default=None,
        const=None
    )
    # TODO: add a --list feature that shows the cryptos that would be used

    args = args_parser.parse_args()
    if args.debug:
        _log.setLevel("DEBUG")
        _log.debug("DEBUG OUTPUT ENABLED!")
    _log.debug(f"args = {args}")

    # Load the CMC APY Key from file
    if not os.path.exists(source_api_key_file):
        _log.error(
            f"No key file found at '{source_api_key_file}'. Do you already have a CoinMarketCap API key? Visit https://coinmarketcap.com/api/ to obtain one.")
        exit(1)
    with open(source_api_key_file) as keyfile:
        api_key: str = keyfile.readline().strip()
        _log.info(f"Successfully loaded API key file!")

    # TODO: replace with actual beancount data objects from the beancount.core
    beancount_string: str = ""

    if source_url == "localtest":
        # TODO: needs to be re-written to use the data stored under ./tests/sample_data
        # _log.warning("Requesting local test mock data...")
        # resp_json = dict(
        #     {
        #         "id": "bitcoin",
        #         "name": "Bitcoin",
        #         "symbol": "BTC",
        #         "rank": "1",
        #         "price_usd": "11347.6",
        #         "price_btc": "1.0",
        #         "24h_volume_usd": "6737540000.0",
        #         "market_cap_usd": "189711446320",
        #         "available_supply": "16718200.0",
        #         "total_supply": "16718200.0",
        #         "max_supply": "21000000.0",
        #         "percent_change_1h": "0.68",
        #         "percent_change_24h": "2.51",
        #         "percent_change_7d": "19.29",
        #         "last_updated": "1512348853"
        #     }
        # )
        _log.warning("Not implemented!")
    else:
        _log.info(
            f"Requesting current cryptocurrency data from: {source_name}")
        # Build up REST query URL
        coinList = ",".join(coinsMap.keys())
        # TODO: use urllib to build the URL
        completed_source_url = f"{source_url}?CMC_PRO_API_KEY={api_key}&symbol={coinList}"
        _log.debug(f"completed_source_url = {completed_source_url}")
        response = requests.get(completed_source_url)
        _log.debug(f"response = {response}")
        # TODO: gracefully handle errors from the endpoint
        resp_json = response.json()['data']
        _log.debug(f"resp_json = {resp_json}")

    result_list: list(str) = []
    coins = coinsMap.keys()
    failedCoins: list(str) = []

    # First, loop through the coins we support according to config
    for coin in coins:
        _log.debug(f"Working on: {coin}")
        date_string: str = query_time.format("YYYY-MM-DD")
        time_string: str = query_time.format("HH:mm:ssZZ")
        # Need to find the right entry in the JSON array
        found: bool = False
        # Loop through the coins in the response from the server
        for coin_entry in resp_json:
            _log.debug(f"Inside loop is at: {coin_entry}")
            if coin_entry == coin:
                found = True
                entry_json = resp_json[coin_entry]
                _log.debug(f"entry_json = {entry_json}")
                price_string_raw = entry_json['quote']['USD']['price']
                _log.debug(f"price_string_raw = {price_string_raw}")
                price_string = f"{float(price_string_raw):.4f}"
                _log.debug(f"    1 {coinsMap[coin]} = {price_string} USD")
                beancount_string = (
                    f"{date_string} price {coinsMap[coin]}    {price_string} USD"
                    f"\n  time: \"{time_string}\""
                    f"\n  source: \"{source_name}\""
                )
                _log.debug(beancount_string)
                result_list.append(beancount_string)
                break

        if not found:
            _log.warning(f"Could not find coin: {coin}")
            failedCoins.append(coin)

        # print("\n") # improves terminal readability

    # Print out the result to the terminal/stdout
    print(" ---- RESULT OUTPUT ----")
    print("")
    for result in result_list:
        print(f"{result}")
    print("")

    # Print out failed coins, if any
    if len(failedCoins) != 0:
        _log.warning("The following coins failed to update:")
        for failedCoin in failedCoins:
            _log.warning(f"{failedCoin}")

    # If user specified save/output_file option, then write out to file now
    if args.output_file:
        with args.output_file as of:
            of.write("\n")
            of.write("\n")
            for result in result_list:
                of.write(f"{result}\n")
            of.write("\n")
            _log.info(
                f"Results saved to Beancount data file: {args.output_file}")


if __name__ == "__main__":
    main()
