#!/usr/bin/env python3

"""Betterment.com CSV File Importer.

==== Usage ====
Visit https://www.betterment.com and view one of your investment funds.
Click on the "Activity" tab, and then adjust the Dates for the desired 
period. When ready, click the "Download CSV" link to get a CSV file to feed
to this Python script!

==== IMPORTANT NOTE ====
This script currently uses some hardcoded values which you will want to modify!
Please review this script to make sure you understand what it is doing in order
to make effective use out of it.

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

from beancount_entry import BeancountEntry
from beancount_metadata import BeancountMetadata
from beancount_posting import BeancountPosting

import argparse
import sys
import logging
from csv import DictReader

import arrow

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(sys.argv[0])


def main():
    argparser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description="",
        usage="")
    argparser.add_argument("input_csv_file",
        help="The CSV file downloaded from the Betterment website."
    )
    
    args = argparser.parse_args()

    bc_entries = list()
    with open(args.input_csv_file) as csv_file:
        header_keys = [h.strip() for h in csv_file.readline().split(',')]
        reader = DictReader(csv_file, fieldnames=header_keys)
        logger.debug("CSV header keys: " + str(reader.fieldnames))
        for line in reader:
            logging.debug(f"Current line: {line}")
            if len(line) == 0:
                continue
            bce = BeancountEntry()
            bce.addMetadata(BeancountMetadata(
                key="imported",
                value=True))

            dateCreated = None
            if line["Date Created"].strip():
                dateCreated = arrow.get(line["Date Created"], "YYYY-MM-DD HH:mm:ss Z")
            else:
                dateCreated = ""
            bce.addMetadata(BeancountMetadata(
                key="dateCreated",
                value=str(dateCreated)
            ))
            
            dateCompleted = None
            if line["Date Completed"].strip():
                dateCompleted = arrow.get(line["Date Completed"], "YYYY-MM-DD HH:mm:ss Z")
                
            else:
                # if Date Completed isn't set yet, then use Date Created
                dateCompleted = dateCreated
            bce.Datetime = dateCompleted

            bce.Flag = "*"
            bce.Payee = "Betterment"
            bce.Description = line["Transaction Description"].strip()
            
            posting1 = BeancountPosting()
            # TODO: move to config file
            # TODO: change to a map in case we have multiple Betterment accounts
            posting1.Account = "Assets:Investments:Betterment:Home-Downpayment"
            posting1.AmountValue = float(line["Amount"])
            posting1.AmountUnit = "USD"

            bce.addPosting(posting1)

            posting2 = BeancountPosting()
            # TODO: this should be moved to a config file
            posting2_map = {
                "Dividend Reinvestment": "Income:Investments:Dividends",
                "Advisory Fee": "Expenses:Financial:Commissions",
                "Automatic Deposit": "Assets:Banking:Ally:Checking",
                "Pending Deposit from ******8105": "Assets:Banking:Ally:Checking"
            }
            posting2.Account = posting2_map[line["Transaction Description"].strip()]
            posting2.AmountValue = -1.0 * float(line["Amount"])
            posting2.AmountUnit = "USD"
            bce.addPosting(posting2)
            
            bc_entries.append(bce)

            # TODO: at the start and at the end, generate balance directives
    
    logger.info("Done processing CSV file!")
    print("")
    for bce in bc_entries:
        print(f"{bce}")
    print("")


if __name__ == '__main__':
    main()
