#! python

import csv, datetime, argparse

# The Mint CSV transaction file is a massive collection of all the transaction
# data they've pulled in from the user's linked accounts. We need to map some
# of the data to the way we represent it in beancount. If a map entry does not
# match, the Mint transaction will be skipped.

# TODOs
# - Match and update with existing transactions in beancount file
# - Interleve transactions into beancount file by date
# - Interactive mode that allows user to specify various fields

# From Mint -> Beancount
accountMap = {
    "CHASE MARSHALL": "Liabilities:Credit-Cards:Chase-Sapphire-Reserve",
    #"Interest Checking": "Assets:Banking:Ally:Checking",
    #"Online Savings": "Assets:Banking:Ally:Savings",
    "Blue Cash Everyday": "Liabilities:Credit-Cards:AMEX-Blue-Card",
}

# Category map takes Mint Categories and maps them to Beancount accounts, typically
# either in the Income or Expense hierarchies. If a match is not found, then
# it will go into the DEFAULT_CATEGORY.
catMap = {
    "Coffee Shops": "Expenses:Consumption:Dining-Out",
    "Fast Food": "Expenses:Consumption:Dining-Out",
    "Gas & Fuel": "Expenses:Automobile:Fuel",
    "Shopping": "Expenses:Shopping",
    "Hotel": "Expenses:Travel:Hotel",
    "Restaurants": "Expenses:Consumption:Dining-Out",
    "Groceries": "Expenses:Consumption:Groceries",
    "Gym": "Expenses:Health:Gym-Membership",
    "Transfer": "",
    "Mobile Phone": "Expenses:Bills:Cell-Phone",
    "Mortgage & Rent": "Expenses:Bills:Mortgage-and-Rent",
    "Paycheck": "Income:Paycheck",
    "Alcohol & Bars": "Expenses:Consumption:Alcohol-and-Bars",
    "Entertainment": "Expenses:Entertainment",
}
DEFAULT_CATEGORY = "Expenses:Other"

mintFile = "C:/Users/arf41/Downloads/transactions (1).csv"
startDate = "2017-09-14"
beancountFile = "./ledger.beancount"

# Program begins
parser = argparse.ArgumentParser();
parser.add_argument("--save",
    help="Saves the results to the beancount ledger file.",
    action="store_true");
args = parser.parse_args()

# Basic beancount transactions look like this:
#     2017-09-14 * "Chase" "CSR CC payment"
#         Assets:Banking:Ally:Checking                                               -3204.00 USD
#         Liabilities:Credit-Cards:Chase-Sapphire-Reserve
skipOld = 0
skipNoAccount = 0
skippedMintAccounts = set()
aggregatedBeancountArray = []
with open(mintFile, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for line in reader:
        dateObj = datetime.datetime.strptime(line['Date'], "%m/%d/%Y")
        startDateObj = datetime.datetime.strptime(startDate, "%Y-%m-%d")
        dateStr = dateObj.strftime("%Y-%m-%d")
        if (dateObj - startDateObj).total_seconds() >= 0:
            entity = line['Description']
            description = line['Original Description']
            if line['Account Name'] in accountMap:
                fromAccount = accountMap[line['Account Name']]
                toAccount = catMap.get(line['Category'], DEFAULT_CATEGORY)
                amount = line['Amount']
                if line['Transaction Type'] == "debit":
                    amount = -1 * float(amount)
                beancountStr = dateStr + " * \"" + entity + "\" \"" + description + "\"\n" + \
                    "  " + fromAccount + "    " + str(amount) + " USD\n" + \
                    "  " + toAccount + "\n"
                print(beancountStr)
                aggregatedBeancountArray.append(beancountStr)
            else:
                skipNoAccount += 1
                skippedMintAccounts.add(line['Account Name'])
        else:
            skipOld += 1

if args.save:
    with open(beancountFile, "a") as fh:
        aggregatedBeancountArray.reverse()
        fh.write("\n");
        for line in aggregatedBeancountArray:
            fh.write(line + "\n");
    print("Results saved to beancount ledger file.");

print("Skipped " + str(skipOld) + " transactions because they were too old.")
print("Skipped " + str(skipNoAccount) + " transactions because they had no matching account.")
if (skipNoAccount > 0):
    print(skippedMintAccounts)
