#! python

# @version ${VERSION}
# @status Usable

# Update All script combines calls to multiple scripts that are mature enough
# to be used to update the Beancount file.
#
# @author Alex Richard Ford (arf4188@gmail.com)
# @website http://www.alexrichardford.com
# @license MIT License

import os, argparse, sys

import update_stocks as usk
import update_crypto_prices as ucp

VERSION="0.2.0"

def main():
    ucp.main()
    # usk.main()

if __name__ == "__main__":
    # Parse arguments
    argParser = argparse.ArgumentParser(
        description="Calls multiple of the update scripts in sequence, as a "
                    "convenience! The results are written to stdout by default, "
                    "unless other options are used.",
        parents=[ucp._parser, usk._parser],
        conflict_handler="resolve")
    argParser.version = "${VERSION}"
    args = argParser.parse_args()
    main()
