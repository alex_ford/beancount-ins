# This Python script will load in the user's Beancount data file
# and look for accounts that have been associated with an Ethereum
# address. The identified accounts will then be synchronized with
# their respective blockchain data recorded for the given address.

# Features/TODO
# [] Parse Beancount data file
# [] Identify accounts with Ethereum addresses
# [] Verifies the address as being Ethereum and not something else
# [] Allows for multiple addresses on a single Beancount account
# [] Pulls down transaction data for those addresses from Ethereum blockchain
# [] Reconciles transactions in Beancount data file with the ones pulled in
# [] Provides interactive mode for reviewing new/updated/conflicting transactions