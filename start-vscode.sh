#!/bin/bash

# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')

if [ -z ${BASH_SOURCE} ]; then
    WKSP_HOME_DIR="$(dirname $(realpath ${0}))"
else
    WKSP_HOME_DIR="$(dirname $(realpath ${BASH_SOURCE}))"
fi

if [ "${DBUG}" == 1 ]; then echo "[ DBUG ] BUNDLE_SCRIPTS_DIR is: ${WKSP_HOME_DIR}"; fi 

ORIGIN_DIR="$(pwd)"
cd "${WKSP_HOME_DIR}" &&
    echo "[ INFO ] Launching VS Code workspace!" &&
    code --verbose \
        --user-data-dir "./.vscode/user-data/" \
        --extensions-dir "./.vscode/extensions/" \
        beancount-ins.code-workspace
cd "${ORIGIN_DIR}"

