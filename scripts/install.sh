#!/bin/bash

ORIGIN_DIR="$(pwd)"
INS_DIR="$(dirname $(dirname $(realpath $BASH_SOURCE)))"
INS_SRC_DIR="${INS_DIR}/beancount_ins/"
INS_SCRIPTS_DIR="${INS_DIR}/scripts/"

SUDO="$(which sudo)"
if [[ "${SUDO}" == *termux* ]]; then
    echo "Termux environment detected! No need to use 'sudo' prefix command."
    SUDO=""
fi

BIN_DIR="/usr/local/bin"
echo "[ INFO ] trying install location: ${BIN_DIR}"
if [ ! -e "${BIN_DIR}" ]; then
    echo "[ WARN ] install location '${BIN_DIR}' doesn't exist!"
    BIN_DIR="$(dirname $(which bash))"
    echo "[ INFO ] trying install location: ${BIN_DIR}"
    if [ ! -e "${BIN_DIR}" ]; then
        echo "[ ERRO ] failed to resolve a suitable location for installation!"
	exit 1
    fi
fi
echo "[ INFO ] installing to: ${BIN_DIR}"

LINK_DESTINATION="${BIN_DIR}/bean-update"

if [ -h "${LINK_DESTINATION}" ]; then
    echo "[ WARN ] destination already exists! Will remove old link first."
    ${SUDO} unlink "${LINK_DESTINATION}"
    if [ $? != 0 ]; then
        echo "[ ERRO ] unlinking has failed! Install incomplete."
        exit 1
    fi
fi
${SUDO} ln -vs "${INS_SCRIPTS_DIR}/bean-update.sh" "${LINK_DESTINATION}"

echo "[ INFO ] preparing Python virtual environment..."

pipenv install

echo "[ INFO ] installation has completed."
echo "[ INFO ] the following have been installed:"
echo "    - bean-update"
echo ""

