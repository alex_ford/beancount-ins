function bean-update() {
    ORIGIN_DIR="$(pwd)"
    INS_DIR="$(dirname $(dirname $(realpath $BASH_SOURCE)))"
    cd ${INS_DIR} &&
        pipenv run python ins/update_crypto_prices.py -s ../beancount-data/other-data/prices.beancount &&
        pipenv run python ins/update_stock_prices.py -s ../beancount-data/other-data/prices.beancount &&
        cd ${ORIGIN_DIR}
}

