$SCRIPTS_DIR    = $PSScriptRoot
$PROJECT_DIR    = Split-Path -Parent "${SCRIPTS_DIR}"
$BEANCOUNT_GROUP_DIR = Split-Path -Parent "${PROJECT_DIR}"
$DATA_DIR       = Resolve-Path "${BEANCOUNT_GROUP_DIR}\beancount-data\"

$PRICES_DATA_FILE = Resolve-Path "${DATA_DIR}\other-data\prices.beancount"

# Resolve the current Beancount file
$CWD = Get-Location
Set-Location "${PROJECT_DIR}"

pipenv run update-crypto-prices --save ${PRICES_DATA_FILE}
pipenv run update-stock-prices --save ${PRICES_DATA_FILE}

Set-Location "${CWD}"
