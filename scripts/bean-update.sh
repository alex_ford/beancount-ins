#!/bin/bash

### bean-update.sh
### Run script for the `bean-update` command line tool. 
###
### Usage:
###    source ~/Workspaces/Personal/beancount/beancount-arfsrc/beancount_arfsrc/beancount_arfsrc.sh
###
### Author: Alex Richard Ford <arf4188@gmail.com>

ORIGIN_DIR="$(pwd)"
INS_DIR="$(dirname $(dirname $(realpath $BASH_SOURCE)))"
INS_SRC_DIR="${INS_DIR}/beancount_ins/"

which bean-config >/dev/null
if [ $? != 0 ]; then
    echo "[ ERRO ] bean-config not found!"
    exit 1
fi
PRICES_FILE="$(bean-config data_dir)/other-data/prices.beancount"
if [ ! -f "${PRICES_FILE}" ]; then
    echo "[ ERRO ] prices file not found at: ${PRICES_FILE}"
    exit 1
fi

cd ${INS_DIR} &&
    pipenv run python ${INS_SRC_DIR}/update_crypto_prices.py -s "${PRICES_FILE}" &&
    pipenv run python ${INS_SRC_DIR}/update_stock_prices.py -s "${PRICES_FILE}"
cd ${ORIGIN_DIR}

