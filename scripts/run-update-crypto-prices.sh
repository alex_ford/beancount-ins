#!/bin/bash

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

CWD="$(pwd)"

cd "${PROJECT_DIR}"

pipenv run python ./beancount_ins/update_crypto_prices.py -s $(bean-config data_dir)/other-data/prices.beancount

cd "${CWD}"
