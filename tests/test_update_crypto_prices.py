#!/usr/bin/env python3

"""Provides local (no Internet) mock data for update_crypto_prices.py to work
with.

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License (see LICENSE file)
"""

import pytest

from beancount_ins import update_crypto_prices

def test_update_crypto_prices():
    update_crypto_prices.sourceUrl = "localtest"
    update_crypto_prices.main()
