# Todo

This list is in addition to any TODOs which may exist inline with the source code. If using an IDE such as [Visual Studio Code](https://code.visualstudio.com) it is highly recommended to use a plugin that searches for TODOs in arbitrary text files located in a given source repository.

One such plugin/extension is:

```
Name: Todo Tree
Id: gruntfuggly.todo-tree
Description: Show TODO, FIXME, etc. comment tags in a tree view
Version: 0.0.130
Publisher: Gruntfuggly
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree
```

## List

- TODO: have `update-stock-prices` pull the list of stock symbols for which price data is fetched from the user's `../beancount-data/configuration/commodities.beancount` "configuration" file.
